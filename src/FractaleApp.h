/*
 * FractaleApp.h
 *
 *  Created on: Mar 8, 2014
 *      Author: piouff_b
 */

#ifndef FRACTALEAPP_H_
#define FRACTALEAPP_H_

#include "ofMain.h"
#include "Range.hpp"
#include "MaxColors.h"

#define MANDELBOT_X_MIN -2.5
#define MANDELBOT_X_MAX 1.0
#define MANDELBOT_Y_MIN -1.5
#define MANDELBOT_Y_MAX 1.5
#define ITERATIONS_MAX 1000000000
#define ITERATIONS_INTERVAL 100

class FractaleApp : public ofSimpleApp {
	//TODO: réunir les paramètres dans une classe séparée
	ofImage 		_image;
	int				_maxIterations;
	int				_targetFPS;
	bool			_pause;
	Range<double>	_horizontalRange;
	Range<double>	_verticalRange;
	double			_zoomFactor;
	double			_moveFactor;
	double			_minIteration;
	MaxColors		_maxColors;
	double			_randomFactor;
public:
	FractaleApp();
	virtual ~FractaleApp();

	void setup();
	void update();
	void draw();
	void keyPressed(int key);
	void windowResized(int w, int h);
private:
	/**
	 * Fractals generation methods
	 */
	int julia(const ofPoint& imgPos);
	int juliaWithRandomFactor(const ofPoint& imgPos);

	void genPixelColor(ofColor& color, int iteration);

	/**
	 * Fractals utils
	 */
	void zoom(double factor);
	void moveRight(double factor);
	void moveLeft(double factor);
	void moveUp(double factor);
	void moveDown(double factor);
};

#endif /* FRACTALEAPP_H_ */
