#pragma once

#include <cstdlib>
#include "ofMain.h"
#include "ofArduino.h"

#define NB_LEDS	6

class mainApp : public ofSimpleApp {
	ofSoundPlayer	_player;
	//ofSoundStream	_stream;
	ofArduino		_arduino;
	bool			_isArduinoInitialized;
	int				_pins[NB_LEDS];
	float			_soundData[NB_LEDS];
	int				_width;
	int				_height;
	int				_colSize;
public:
		void setup();
		void update();
		void draw();
		//void audioIn(float* input, int bufferSize, int nbChannels);

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y);
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		void exit();
private:
		void setupArduino(const int & version);
		void updateArduino();
};
