/*
 * MaxColors.h
 *
 *  Created on: Mar 19, 2014
 *      Author: piouff_b
 */

#ifndef MAXCOLORS_H_
#define MAXCOLORS_H_

#include <ostream>

class MaxColors {
public:
	int r, g, b;
	MaxColors(int, int, int);
	virtual ~MaxColors();

	int& operator[](int pos);
};

std::ostream& operator<<(std::ostream& stream, MaxColors& self);

#endif /* MAXCOLORS_H_ */
