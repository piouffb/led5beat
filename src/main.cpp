#include "ofMain.h"
#include "ofAppGlutWindow.h"
//#include "mainApp.h"
//#include "FractaleApp.h"
#include "FractaleMusicApp.h"

int main( ){
	//ofSetLogLevel(OF_LOG_WARNING);

	/*
	 * Fractale App
	 */
	//ofAppGlutWindow window;
	//ofSetupOpenGL(&window, 1024,768,OF_WINDOW);
	//ofRunApp(new FractaleApp());

	/*
	 * Or Fractale music App
	 */
	ofAppGlutWindow window;
	ofSetupOpenGL(&window, 600, 400, OF_WINDOW);
	ofRunApp(new FractaleMusicApp());

	/*
	 * Or Arduino app
	 */
	//ofSetupOpenGL(1024,768, OF_WINDOW);
	//ofRunApp(new mainApp());
}
