/*
 * FractaleApp.cpp
 *
 *  Created on: Mar 8, 2014
 *      Author: piouff_b
 */

#include "FractaleApp.h"

FractaleApp::FractaleApp() : _maxIterations(10), _targetFPS(10), _pause(false), _horizontalRange(MANDELBOT_X_MIN, MANDELBOT_X_MAX),
							 _verticalRange(MANDELBOT_Y_MIN, MANDELBOT_Y_MAX), _zoomFactor(0.0), _moveFactor(0.01), _minIteration(0),
							 _maxColors(255, 255, 255), _randomFactor(1.0) {
}

FractaleApp::~FractaleApp() {
}

void FractaleApp::setup() {
	ofSetFrameRate(10);
	ofBackground(0);
}

int FractaleApp::julia(const ofPoint& imgPos) {
	double x0 = ofMap(imgPos.x, 0, _image.getWidth(), _horizontalRange.getMin(), _horizontalRange.getMax());
	double y0 = ofMap(imgPos.y, 0, _image.getHeight(), _verticalRange.getMin(), _verticalRange.getMax());

	double x = 0.0;
	double y = 0.0;
	int iteration = _minIteration;

	while (x * x + y * y < 2.0 * 2.0 && iteration < _maxIterations) {
		double xtemp = x * x - y * y + x0;
		y = 2 * x * y + y0;
		x = xtemp;
		iteration = iteration + 1;
	}
	return iteration;
}

int FractaleApp::juliaWithRandomFactor(const ofPoint& imgPos) {
	double x0 = ofMap(imgPos.x, 0, _image.getWidth(), _horizontalRange.getMin(), _horizontalRange.getMax());
	double y0 = ofMap(imgPos.y, 0, _image.getHeight(), _verticalRange.getMin(), _verticalRange.getMax());

	double x = 0.0;
	double y = 0.0;
	int iteration = _minIteration;

	while (x * x + y * y < 2.0 * 2.0 && iteration < _maxIterations) {
		double xtemp = x * x - y * y + x0;
		y = 2 * x * y + y0;
		x = xtemp; //TODO: implementer random factor ici
		iteration = iteration + 1;
	}
	return iteration;
}

void FractaleApp::zoom(double factor) {
	double horizontalZoom = abs(_horizontalRange.getMax() - _horizontalRange.getMin()) * factor;
	double verticalZoom = abs(_verticalRange.getMax() - _verticalRange.getMin()) * factor;

	this->_horizontalRange.setMin(this->_horizontalRange.getMin() + horizontalZoom);
	this->_horizontalRange.setMax(this->_horizontalRange.getMax() - horizontalZoom);
	this->_verticalRange.setMin(this->_verticalRange.getMin() + verticalZoom);
	this->_verticalRange.setMax(this->_verticalRange.getMax() - verticalZoom);
}

void FractaleApp::moveRight(double factor) {
	//TODO: Checker si les valeurs sortent pas de l'intervalle
	double moveValue = abs(_horizontalRange.getMax() - _horizontalRange.getMin()) * factor;
	this->_horizontalRange.setMin(this->_horizontalRange.getMin() + moveValue);
	this->_horizontalRange.setMax(this->_horizontalRange.getMax() + moveValue);
}

void FractaleApp::moveLeft(double factor) {
	double moveValue = abs(_horizontalRange.getMax() - _horizontalRange.getMin()) * factor;
	this->_horizontalRange.setMin(this->_horizontalRange.getMin() - moveValue);
	this->_horizontalRange.setMax(this->_horizontalRange.getMax() - moveValue);
}

void FractaleApp::moveUp(double factor) {
	double moveValue = abs(_horizontalRange.getMax() - _horizontalRange.getMin()) * factor;
	this->_verticalRange.setMin(this->_verticalRange.getMin() - moveValue);
	this->_verticalRange.setMax(this->_verticalRange.getMax() - moveValue);
}

void FractaleApp::moveDown(double factor) {
	double moveValue = abs(_horizontalRange.getMax() - _horizontalRange.getMin()) * factor;
	this->_verticalRange.setMin(this->_verticalRange.getMin() + moveValue);
	this->_verticalRange.setMax(this->_verticalRange.getMax() + moveValue);
}

void FractaleApp::genPixelColor(ofColor& color, int iteration) {
	for (int i = 0; i < 3; i++) {
		if (this->_maxColors[i] >= 0) {
			color[i] = ofMap(iteration, _minIteration, _maxIterations, 0, this->_maxColors[i]);
		} else {
			color[i] = ofRandom(0, abs(this->_maxColors[i]));
		}
	}
}

void FractaleApp::update() {
	if (!this->_pause) {
		ofColor color;
		int roundMinIteration = ITERATIONS_MAX;
		for (int y = 0; y < _image.getHeight(); y++) {
			for (int x = 0; x < _image.getWidth(); x++) {
				int iteration = julia(ofPoint(x, y));
				if (iteration < roundMinIteration) {
					roundMinIteration = iteration;
				}
				genPixelColor(color, iteration);
				_image.setColor(x, y, color);
			}
		}
		if (roundMinIteration > _minIteration && roundMinIteration != ITERATIONS_MAX) {
			_minIteration = roundMinIteration;
			_maxIterations = _minIteration + ITERATIONS_INTERVAL;
		}
		_image.update();
		if (_zoomFactor) {
			zoom(_zoomFactor);
		}
	}
}

void FractaleApp::draw() {
	if (!this->_pause) {
		_image.draw(0, 0);
	}
}

void FractaleApp::keyPressed(int key) {
	/* Iterations */
	if (key == 'i' && this->_maxIterations > 1) {
		ofLog(OF_LOG_NOTICE, "Max iterations: " + ofToString(--this->_maxIterations));
	} else if (key == 'o' && this->_maxIterations < ITERATIONS_MAX) {
		ofLog(OF_LOG_NOTICE, "Max iterations: " + ofToString(++this->_maxIterations));
	}
	/* FPS */
	else if (key == 'f' && this->_targetFPS > 0) {
		ofSetFrameRate(--this->_targetFPS);
		ofLog(OF_LOG_NOTICE, "Target FPS: " + (this->_targetFPS ? ofToString(this->_targetFPS) : "Unlimited"));
	} else if (key == 'g' && this->_targetFPS < 500) {
		ofSetFrameRate(++this->_targetFPS);
		ofLog(OF_LOG_NOTICE, "Target FPS: " + ofToString(this->_targetFPS));
	}
	/* Pause */
	else if (key == ' ') {
		bool paused = this->_pause = !this->_pause;
		ofLog(OF_LOG_NOTICE, ((paused) ? "Paused" : "Continue"));
	}
	/* Zoom */
	else if (key == 'z') {
		this->_zoomFactor -= 0.001;
		ofLog(OF_LOG_NOTICE, "Zoom factor: " + ofToString(this->_zoomFactor));
	} else if (key == 'e') {
		this->_zoomFactor += 0.001;
		ofLog(OF_LOG_NOTICE, "Zoom factor: " + ofToString(this->_zoomFactor));
	}
	/* Move */
	else if (key == OF_KEY_LEFT) {
		this->moveLeft(_moveFactor);
	}
	else if (key == OF_KEY_RIGHT) {
		this->moveRight(_moveFactor);
	}
	else if (key == OF_KEY_UP) {
		this->moveUp(_moveFactor);
	}
	else if (key == OF_KEY_DOWN) {
		this->moveDown(_moveFactor);
	}
	/* Fullscreen */
	else if (key == OF_KEY_F11) {
		ofToggleFullscreen();
	}
	/* Screenshot */
	else if (key == 'p') {
		std::string filename = "led05beat_" + ofGetTimestampString() + ".png";
		this->_image.saveImage(filename, OF_IMAGE_QUALITY_BEST);
		ofLog(OF_LOG_NOTICE, "Screenshoot saved as " + filename);
	}
	/* Colors */
	else if (key >= '4' && key <= '9') {
		if (key == '4') {
			this->_maxColors.r = this->_maxColors.r > -255 ? this->_maxColors.r - 1 : -255;
		} else if (key == '7') {
			this->_maxColors.r = this->_maxColors.r < 255 ? this->_maxColors.r + 1 : 255;
		} else if (key == '5') {
			this->_maxColors.g = this->_maxColors.g > -255 ? this->_maxColors.g - 1 : -255;
		} else if (key == '8') {
			this->_maxColors.g = this->_maxColors.g < 255 ? this->_maxColors.g + 1 : 255;
		} else if (key == '6') {
			this->_maxColors.b = this->_maxColors.b > -255 ? this->_maxColors.b - 1 : -255;
		} else if (key == '9') {
			this->_maxColors.b = this->_maxColors.b < 255 ? this->_maxColors.b + 1 : 255;
		}
		ofLog(OF_LOG_NOTICE, "Colors: " + ofToString((int)this->_maxColors.r) + '|' + ofToString((int)this->_maxColors.g) + '|'  + ofToString((int)this->_maxColors.b));
	}
	/* Random factor */
	else if (key == 'r') {
		_randomFactor -= 0.001;
		ofLog(OF_LOG_NOTICE, "Random factor : " + ofToString(_randomFactor));
	} else if (key == 't') {
		_randomFactor += 0.001;
		ofLog(OF_LOG_NOTICE, "Random factor : " + ofToString(_randomFactor));
	}
}

void FractaleApp::windowResized(int w, int h) {
	this->_pause = true; //TODO: Mettre un vrai système de lock
	this->_image.clear();
	_image.allocate(w, h, OF_IMAGE_COLOR);
	this->_pause = false;
}
