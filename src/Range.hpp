/*
 * Range.h
 *
 *  Created on: Mar 9, 2014
 *      Author: piouff_b
 */

#ifndef RANGE_H_
#define RANGE_H_

#include <ostream>

template<typename T>
class Range {
	T _min;
	T _max;
public:
	Range(T min, T max) : _min(min), _max(max) {
	}
	~Range() {
	}

	T getMax() const {
		return _max;
	}

	void setMax(T max) {
		_max = max;
	}

	T getMin() const {
		return _min;
	}

	void setMin(T min) {
		_min = min;
	}

};

template<typename T>
std::ostream& operator<<(std::ostream& stream, const Range<T>& range) {
	stream << "[" << range.getMin() << "," << range.getMax() << "]";
	return stream;
}


#endif /* RANGE_H_ */
