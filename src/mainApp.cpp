#include "mainApp.h"

void mainApp::setup()
{
	const std::string arduinoCOMPort = "/dev/ttyACM0";
	const std::string soundFilename = "sound.mp3";

	ofSetFrameRate(25);
	ofBackground(0);
	this->windowResized(ofGetViewportWidth(), ofGetViewportHeight());

	this->_pins[0] = 3;
	this->_pins[1] = 5;
	this->_pins[2] = 6;
	this->_pins[3] = 9;
	this->_pins[4] = 10;
	this->_pins[5] = 11;
	this->_isArduinoInitialized = false;

	for(int i = 0; i < NB_LEDS; i++) {
		_soundData[i] = 0.0;
	}

	if (this->_player.loadSound(soundFilename) && this->_arduino.connect(arduinoCOMPort, 57600)) {
		ofAddListener(_arduino.EInitialized, this, &mainApp::setupArduino);
	} else {
		//std::exit(0);
	}
}

void mainApp::setupArduino(const int & version) {
	ofRemoveListener(_arduino.EInitialized, this, &mainApp::setupArduino);

	for (int i = 0; i < NB_LEDS; i++) {
		ofSleepMillis(100);
		this->_arduino.sendDigitalPinMode(this->_pins[i], ARD_PWM);
		ofLog(OF_LOG_NOTICE, "Binded pin " + ofToString(_pins[i]));
	}
	ofLog(OF_LOG_NOTICE, "Arduino initialized");
	this->_isArduinoInitialized = true;
	this->_player.play();
}

void mainApp::exit() {
	this->_arduino.disconnect();
	this->_player.unloadSound();
	ofSoundShutdown();
}

void mainApp::updateArduino() {
	this->_arduino.update();
	if (this->_isArduinoInitialized) {
		const double volume = this->_player.getVolume();
		const float* data = ofSoundGetSpectrum(NB_LEDS);

		this->_soundData[0] = data[0] / 2.0;
		for (int i = 0; i < NB_LEDS; i++) {
			if (i) {
				this->_soundData[i] = data[i] * i; // Trying to normalize volume...
			}
			int brigthness = (int)ofMap(_soundData[i], 0.0, (_soundData[i] > volume ? _soundData[i] : volume), 0.0, 255.0);
			this->_arduino.sendPwm(this->_pins[i], brigthness);
		}
	}
}

void mainApp::update() {
	ofSoundUpdate();
	updateArduino();
}

//--------------------------------------------------------------
void mainApp::draw()
{
	for (int i = 0; i < NB_LEDS; i++) {
		ofRect(this->_colSize + (i * this->_colSize), this->_height,
				this->_colSize, -(this->_soundData[i] * this->_height));
	}
}


//--------------------------------------------------------------
void mainApp::keyPressed  (int key)
{
	if (key == '+' && this->_player.getVolume() < 1.0) {
		this->_player.setVolume(this->_player.getVolume() + 0.1);
	}
	else if (key == '-' && this->_player.getVolume() > 0.0) {
		this->_player.setVolume(this->_player.getVolume() - 0.1);
	}
}

void mainApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void mainApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void mainApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void mainApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void mainApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void mainApp::windowResized(int w, int h){
	this->_width = w;
	this->_height = h;
	this->_colSize = this->_width / (NB_LEDS + 2);
	ofLog(OF_LOG_NOTICE, "Window resized: Column of size " + ofToString(_colSize) + " on " + ofToString(_width) + "x" + ofToString(_height) + " window");;
}

//--------------------------------------------------------------
void mainApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void mainApp::dragEvent(ofDragInfo dragInfo){

}
