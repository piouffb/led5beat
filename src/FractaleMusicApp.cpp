/*
 * FractaleMusicApp.cpp
 *
 *  Created on: Mar 8, 2014
 *      Author: piouff_b
 */

#include "FractaleMusicApp.h"

FractaleMusicApp::FractaleMusicApp() : _maxIterations(10), _targetFPS(30), _pause(false), _horizontalRange(MANDELBOT_X_MIN, MANDELBOT_X_MAX),
							 _verticalRange(MANDELBOT_Y_MIN, MANDELBOT_Y_MAX), _zoomFactor(0.0), _moveFactor(0.01), _minIteration(0),
							 _maxColors(255, 255, 255), _randomFactor(0.0), _soundData(0), _musicTrigger(0.5), _smoothness(0), _animate(false) {
}

FractaleMusicApp::~FractaleMusicApp() {
}

void FractaleMusicApp::setup() {
	ofSetFrameRate(_targetFPS);
	ofSetVerticalSync(true);
	ofBackground(0);

	if (this->_player.loadSound("sound.mp3") == false) {
		std::exit(1);
	}
	this->_player.play();
}

int FractaleMusicApp::julia(const ofPoint& imgPos) {
	double x0 = ofMap(imgPos.x, 0, _image.getWidth(), _horizontalRange.getMin(), _horizontalRange.getMax());
	double y0 = ofMap(imgPos.y, 0, _image.getHeight(), _verticalRange.getMin(), _verticalRange.getMax());

	double x = 0.0;
	double y = 0.0;
	int iteration = _minIteration;

	while (x * x + y * y < 2.0 * 2.0 && iteration < _maxIterations) {
		double xtemp = x * x - y * y + x0;
		y = 2 * x * y + y0;
		x = xtemp;
		iteration = iteration + 1;
	}
	return iteration;
}

int FractaleMusicApp::juliaWithRandomFactor(const ofPoint& imgPos) {
	double x0 = ofMap(imgPos.x, 0, _image.getWidth(), _horizontalRange.getMin(), _horizontalRange.getMax());
	double y0 = ofMap(imgPos.y, 0, _image.getHeight(), _verticalRange.getMin(), _verticalRange.getMax());

	double x = 0.0;
	double y = 0.0;
	int iteration = _minIteration;

	while (x * x + y * y < 2.0 * 2.0 && iteration < _maxIterations) {
		double xtemp = x * x - y * y + x0;
		y = 2 * x * y + y0;
		x = xtemp;
		iteration = iteration + 1;
	}
	return iteration;
}

void FractaleMusicApp::zoom(double factor) {
	double horizontalZoom = abs(_horizontalRange.getMax() - _horizontalRange.getMin()) * factor;
	double verticalZoom = abs(_verticalRange.getMax() - _verticalRange.getMin()) * factor;

	this->_horizontalRange.setMin(this->_horizontalRange.getMin() + horizontalZoom);
	this->_horizontalRange.setMax(this->_horizontalRange.getMax() - horizontalZoom);
	this->_verticalRange.setMin(this->_verticalRange.getMin() + verticalZoom);
	this->_verticalRange.setMax(this->_verticalRange.getMax() - verticalZoom);
}

void FractaleMusicApp::moveRight(double factor) {
	//TODO: Checker si les valeurs sortent pas de l'intervalle
	double moveValue = abs(_horizontalRange.getMax() - _horizontalRange.getMin()) * factor;
	this->_horizontalRange.setMin(this->_horizontalRange.getMin() + moveValue);
	this->_horizontalRange.setMax(this->_horizontalRange.getMax() + moveValue);
}

void FractaleMusicApp::moveLeft(double factor) {
	double moveValue = abs(_horizontalRange.getMax() - _horizontalRange.getMin()) * factor;
	this->_horizontalRange.setMin(this->_horizontalRange.getMin() - moveValue);
	this->_horizontalRange.setMax(this->_horizontalRange.getMax() - moveValue);
}

void FractaleMusicApp::moveUp(double factor) {
	double moveValue = abs(_horizontalRange.getMax() - _horizontalRange.getMin()) * factor;
	this->_verticalRange.setMin(this->_verticalRange.getMin() - moveValue);
	this->_verticalRange.setMax(this->_verticalRange.getMax() - moveValue);
}

void FractaleMusicApp::moveDown(double factor) {
	double moveValue = abs(_horizontalRange.getMax() - _horizontalRange.getMin()) * factor;
	this->_verticalRange.setMin(this->_verticalRange.getMin() + moveValue);
	this->_verticalRange.setMax(this->_verticalRange.getMax() + moveValue);
}

void FractaleMusicApp::genPixelColor(ofColor& color, int iteration) {
	for (int i = 0; i < 3; i++) {
		if (this->_maxColors[i] >= 0) {
			color[i] = ofMap(iteration, _minIteration, _maxIterations, 0,
					this->_maxColors[i]);
		} else {
			color[i] = ofRandom(0, abs(this->_maxColors[i]));
		}
		if (_soundData && _soundData[i] > this->_musicTrigger) {
			color[i] = color[i] * this->_soundData[i];
		}
	}
}

void FractaleMusicApp::smooth(ofColor& color, ofColor& prevColor) {
	for (int i = 0; i < 3; i++) {
		if (_smoothness > 0) {
			color[i] = (prevColor[i] * _smoothness + color[i])
					/ (_smoothness + 1);
		} else if (_smoothness < 0) {
			int realSmoothness = abs(_smoothness);
			color[i] = (prevColor[i] + color[i] * realSmoothness)
					/ (realSmoothness + 1);
		}
	}
}

void FractaleMusicApp::animate() {
	static double zoomIncr = 0.0001;
	static moveFunc moveFuncs[4] = {
			&FractaleMusicApp::moveRight, &FractaleMusicApp::moveLeft, &FractaleMusicApp::moveUp, &FractaleMusicApp::moveDown
	};

	if (_zoomFactor > 0.02 || _zoomFactor < -0.020) {
		zoomIncr = -zoomIncr;
	}
	_zoomFactor += zoomIncr;
	(this->*(moveFuncs[(int)ofRandom(0, 3)]))(_moveFactor / 10.0);
}

void FractaleMusicApp::update() {
	if (!this->_pause) {
		ofColor color;
		int roundMinIteration = ITERATIONS_MAX;
		this->_soundData = ofSoundGetSpectrum(NB_SOUND_RANGES);
		for (int y = 0; y < _image.getHeight(); y++) {
			for (int x = 0; x < _image.getWidth(); x++) {
				int iteration = julia(ofPoint(x, y));
				if (iteration < roundMinIteration) {
					roundMinIteration = iteration;
				}
				genPixelColor(color, iteration);
				if (_smoothness) {
					ofColor prevColor = _image.getColor(x, y);
					smooth(color, prevColor);
				}
				_image.setColor(x, y, color);
			}
		}
		if (roundMinIteration > _minIteration && roundMinIteration != ITERATIONS_MAX) {
			_minIteration = roundMinIteration;
			_maxIterations = _minIteration + ITERATIONS_INTERVAL;
		}
		_image.update();
		if (_animate) {
			this->animate();
		}
		if (_zoomFactor) {
			zoom(_zoomFactor);
		}
	}
}

void FractaleMusicApp::draw() {
	if (!this->_pause) {
		_image.draw(0, 0);
	}
}

void FractaleMusicApp::keyPressed(int key) {
	/* Iterations */
	if (key == 'i' && this->_maxIterations > 1) {
		ofLog(OF_LOG_NOTICE, "Max iterations: " + ofToString(--this->_maxIterations));
	} else if (key == 'o' && this->_maxIterations < ITERATIONS_MAX) {
		ofLog(OF_LOG_NOTICE, "Max iterations: " + ofToString(++this->_maxIterations));
	}
	/* FPS */
	else if (key == 'f' && this->_targetFPS > 0) {
		ofSetFrameRate(--this->_targetFPS);
		ofLog(OF_LOG_NOTICE, "Target FPS: " + (this->_targetFPS ? ofToString(this->_targetFPS) : "Unlimited"));
	} else if (key == 'g' && this->_targetFPS < 500) {
		ofSetFrameRate(++this->_targetFPS);
		ofLog(OF_LOG_NOTICE, "Target FPS: " + ofToString(this->_targetFPS));
	}
	/* Pause */
	else if (key == ' ') {
		bool paused = this->_pause = !this->_pause;
		ofLog(OF_LOG_NOTICE, ((paused) ? "Paused" : "Continue"));
	}
	/* Zoom */
	else if (key == 'z') {
		this->_zoomFactor -= 0.001;
		ofLog(OF_LOG_NOTICE, "Zoom factor: " + ofToString(this->_zoomFactor));
	} else if (key == 'e') {
		this->_zoomFactor += 0.001;
		ofLog(OF_LOG_NOTICE, "Zoom factor: " + ofToString(this->_zoomFactor));
	}
	/* Move */
	else if (key == OF_KEY_LEFT) {
		this->moveLeft(_moveFactor);
	}
	else if (key == OF_KEY_RIGHT) {
		this->moveRight(_moveFactor);
	}
	else if (key == OF_KEY_UP) {
		this->moveUp(_moveFactor);
	}
	else if (key == OF_KEY_DOWN) {
		this->moveDown(_moveFactor);
	}
	/* Fullscreen */
	else if (key == OF_KEY_F11) {
		ofToggleFullscreen();
	}
	/* Screenshot */
	else if (key == 'p') {
		std::string filename = "led05beat_" + ofGetTimestampString() + ".png";
		this->_image.saveImage(filename, OF_IMAGE_QUALITY_BEST);
		ofLog(OF_LOG_NOTICE, "Screenshoot saved as " + filename);
	}
	/* Colors */
	else if (key >= '4' && key <= '9') {
		if (key == '4') {
			this->_maxColors.r = this->_maxColors.r > -255 ? this->_maxColors.r - 1 : -255;
		} else if (key == '7') {
			this->_maxColors.r = this->_maxColors.r < 255 ? this->_maxColors.r + 1 : 255;
		} else if (key == '5') {
			this->_maxColors.g = this->_maxColors.g > -255 ? this->_maxColors.g - 1 : -255;
		} else if (key == '8') {
			this->_maxColors.g = this->_maxColors.g < 255 ? this->_maxColors.g + 1 : 255;
		} else if (key == '6') {
			this->_maxColors.b = this->_maxColors.b > -255 ? this->_maxColors.b - 1 : -255;
		} else if (key == '9') {
			this->_maxColors.b = this->_maxColors.b < 255 ? this->_maxColors.b + 1 : 255;
		}
		ofLog(OF_LOG_NOTICE, "Colors: " + ofToString((int)this->_maxColors.r) + '|' + ofToString((int)this->_maxColors.g) + '|'  + ofToString((int)this->_maxColors.b));
	}
	/* Random factor */
	else if (key == 'r') {
		_randomFactor -= 0.001;
		ofLog(OF_LOG_NOTICE, "Random factor : " + ofToString(_randomFactor));
	} else if (key == 't') {
		_randomFactor += 0.001;
		ofLog(OF_LOG_NOTICE, "Random factor : " + ofToString(_randomFactor));
	}
	/* Music trigger */
	else if (key == 'l') {
		_musicTrigger -= 0.01;
		ofLog(OF_LOG_NOTICE, "Music trigger : " + ofToString(_musicTrigger));
	} else if (key == 'm') {
		_musicTrigger += 0.01;
		ofLog(OF_LOG_NOTICE, "Music trigger : " + ofToString(_musicTrigger));
	}
	/* Smoothness */
	else if (key == 's') {
		_smoothness -= 1;
		ofLog(OF_LOG_NOTICE, "Smoothness : " + ofToString(_smoothness));
	} else if (key == 'd') {
		_smoothness += 1;
		ofLog(OF_LOG_NOTICE, "Smoothness : " + ofToString(_smoothness));
	}
	/* Animation */
	else if (key == 'a') {
		this->_animate = !this->_animate;
		ofLog(OF_LOG_NOTICE, "Animate : " + ofToString(this->_animate));
	}
}

void FractaleMusicApp::windowResized(int w, int h) {
	this->_pause = true; //TODO: Mettre un vrai système de lock
	this->_image.clear();
	_image.allocate(w, h, OF_IMAGE_COLOR);
	this->_pause = false;
}
