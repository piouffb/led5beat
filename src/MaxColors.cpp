/*
 * MaxColors.cpp
 *
 *  Created on: Mar 19, 2014
 *      Author: piouff_b
 */

#include "MaxColors.h"

MaxColors::MaxColors(int red, int green, int blue) : r(red), g(green), b(blue) {
}

MaxColors::~MaxColors() {
}

int& MaxColors::operator [](int pos) {
	if (pos == 0) {
		return r;
	}
	if (pos == 1) {
		return g;
	}
	return b;
}

std::ostream& operator <<(std::ostream& stream, MaxColors& self) {
	stream << self.r << '|' << self.g << '|' << self.b;
	return stream;
}
