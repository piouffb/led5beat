/*
 * FractaleMusicApp.h
 *
 *  Created on: Mar 8, 2014
 *      Author: piouff_b
 */

#ifndef FractaleMusicApp_H_
#define FractaleMusicApp_H_

#include "ofMain.h"
#include "Range.hpp"
#include "MaxColors.h"

#define MANDELBOT_X_MIN -2.5
#define MANDELBOT_X_MAX 1.0
#define MANDELBOT_Y_MIN -1.5
#define MANDELBOT_Y_MAX 1.5
#define ITERATIONS_MAX 1000000000
#define ITERATIONS_INTERVAL 100

#define NB_SOUND_RANGES	3

class FractaleMusicApp : public ofSimpleApp {
	//TODO: réunir les paramètres dans une classe séparée
	ofImage 		_image;
	int				_maxIterations;
	int				_targetFPS;
	bool			_pause;
	Range<double>	_horizontalRange;
	Range<double>	_verticalRange;
	double			_zoomFactor;
	double			_moveFactor;
	double			_minIteration;
	MaxColors		_maxColors;
	double			_randomFactor;
	ofSoundPlayer	_player;
	float*			_soundData;
	float			_musicTrigger;
	int				_smoothness;
	bool			_animate;
public:
	FractaleMusicApp();
	virtual ~FractaleMusicApp();

	void setup();
	void update();
	void draw();
	void keyPressed(int key);
	void windowResized(int w, int h);
private:
	/**
	 * Fractals generation methods
	 */
	int julia(const ofPoint& imgPos);
	int juliaWithRandomFactor(const ofPoint& imgPos);

	void genPixelColor(ofColor& color, int iteration);
	void smooth(ofColor& color, ofColor& prevColor);

	/**
	 * Fractals utils
	 */
	void zoom(double factor);
	void animate();
	void moveRight(double factor);
	void moveLeft(double factor);
	void moveUp(double factor);
	void moveDown(double factor);
};

typedef void (FractaleMusicApp::*moveFunc)(double factor);

#endif /* FractaleMusicApp_H_ */
